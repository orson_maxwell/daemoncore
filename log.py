﻿#!/usr/bin/env python
"""
Log module.
"""

import sys

__all__ = [
  'Log'
]

log_levels = {
  'error': 3,
  'warning': 2,
  'info': 1,
  'debug': 0,
}

class Log(object):
  """Log messages."""
  
  level = 'error'
  
  def __init__(self, log_level):
    if str(log_level).lower() in ['error', 'warning', 'info', 'debug']:
      self.level = log_levels[log_level]
  
  def error(self, message):
    if self.level <= 3:
      print('[E] {}'.format(message))
    
  def warning(self, message):
    if self.level <= 2:
      print('[W] {}'.format(message))

  def info(self, message):
    if self.level <= 1:
      print('[I] {}'.format(message))

  def debug(self, message):
    if self.level <= 0:
      print('[D] {}'.format(message))