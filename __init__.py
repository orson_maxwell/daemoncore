﻿#!/usr/bin/env python
"""
Daemoncore engine package.
"""
from __future__ import division

__author__    = 'Orson Maxwell'
__copyright__ = 'Copyright 2013, Orson Maxwell a.k.a. Tech Heathen'
__credits__   = ['Orson Maxwell']
__license__   = 'CC0'
__version__   = '0.0.1'
__date__      = '19.09.2013'
__maintainer__= 'Orson Maxwell'
__email__     = 'orson@techheathen.com'
__status__    = 'Prototype'

from daemoncore import *