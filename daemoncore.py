﻿#!/usr/bin/env python
"""
Daemoncore engine main module.
"""

import pygame

__all__ = [
  'Daemoncore'
]

from pygame import init as pygame_init
from pygame import quit as pygame_quit
from xplatform import *
from log import *

class Daemoncore(object):
  """The main engine class."""

  def __init__(self, *args, **kwargs):
    """Rev the engine."""
    
    if 'log_level' in kwargs.keys():
      self.log = Log(kwargs['log_level'])
    else:
      self.log = Log()
    self.log.info('Starting up Daemoncore')
    self.log.debug('Screen size: {}'.format(get_screen_size()))
    pygame_init()
    self.window = window.Window()

  def __close__():
    """Kill the engine."""
    pygame_quit()
  
  def run(self):
    while True:
      event = pygame.event.poll()
    
      if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
        break