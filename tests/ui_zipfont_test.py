﻿#!/usr/bin/env python
"""
Test program for the ui.zipfont module.
"""

from __future__ import division

__author__    = 'Orson Maxwell'
__copyright__ = 'Copyright 2013, Orson Maxwell a.k.a. Tech Heathen'
__credits__   = ['Orson Maxwell']
__license__   = 'CC0'
__version__   = '0.0.1'
__date__      = '23.10.2013'
__maintainer__= 'Orson Maxwell'
__email__     = 'orson@techheathen.com'
__status__    = 'Performance test'

from sys import path
from os.path import abspath
from os.path import basename
from os.path import isfile
from os.path import join
from os import listdir
path.append(abspath('..'))

import pygame
from pygame.locals import *

from ui import *

def main():
  pygame.init()
  
  clock = pygame.time.Clock()
  
  window = pygame.display.set_mode((1024, 768))
  
  font_dir = './data/fonts'
  fonts = [ffile for ffile in listdir(font_dir) if isfile(join(font_dir, ffile))]
  
  font_number = 0
  
  font = zipfont.ZipFont(join(font_dir, fonts[0]),
                ignore_missing=True)
  
  scale = 1
  
  back_fill = None
  back_tile = None
  back_tile_colour = (80, 0, 0)
  colour = None
  alpha = 255

  fps = '0'
  frames = 0
  dt = 0
  


  pygame.key.set_repeat(300, 60)
  
  while True:
    window.fill((0, 0, 0, 255))
    
    event = pygame.event.poll()
    
    if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
      break
    elif event.type == KEYDOWN:
      if event.key == K_KP_PLUS:
        if scale < 4:
          scale += 0.1
          window.fill((0, 0, 0), sample_rect)
      if event.key == K_KP_MINUS:
        if scale > 0.2:
          scale -= 0.1
          window.fill((0, 0, 0), sample_rect)
      if event.key == K_b:
        if back_fill:
          back_fill = None
        else:
          back_fill = (64, 0, 0)
      if event.key == K_t:
        if back_tile:
          back_tile = None
        else:
          back_tile = unichr(0x2591)
      if event.key == K_UP:
        alpha += 10
        if alpha > 255:
          alpha = 255
      if event.key == K_DOWN:
        alpha -= 10
        if alpha <= 0:
          alpha = 0
      if event.key == K_f:
        font_number += 1
        if font_number >= len(fonts):
          font_number = 0
        font = zipfont.ZipFont(join(font_dir, fonts[font_number]),
                              ignore_missing=True)
    
    colour = (200, 128, 0, alpha)
    
    sample_text = font.render('Sample text about a brown fox.', colour=colour,
                  scale=scale, back_fill=back_fill, back_tile=back_tile,
                  back_tile_colour=back_tile_colour)
    sample_rect = sample_text.get_rect()
    sample_rect.center = window.get_rect().center
    window.blit(sample_text, sample_rect)

    legend1_text = font.render('Press keypad + and - to change scale:' +
                  ' {:1.2f}    ESC to exit'.format(scale))
    legend1_rect = legend1_text.get_rect()
    legend1_rect.topleft = (10, 20)
    window.blit(legend1_text, legend1_rect)
    
    legend2_text = font.render('Press UP and DOWN to change alpha: ' +
                    '{:3d}'.format(alpha))
    legend2_rect = legend2_text.get_rect()
    legend2_rect.topleft = (10, 36)
    window.blit(legend2_text, legend2_rect)
    
    legend3_text = font.render('Press B to toggle background fill and T to toggle background tiles')
    legend3_rect = legend3_text.get_rect()
    legend3_rect.topleft = (10, 52)
    window.blit(legend3_text, legend3_rect)
    
    font_name = font.typeface
    if font.bold:
      font_name += ' bold'
    if font.italic:
      font_name += ' italic'
    legend4_text = font.render('Press F to cycle through fonts: ' +
                              '{}'.format(font_name))
    legend4_rect = legend4_text.get_rect()
    legend4_rect.topleft = (10, 68)
    window.blit(legend4_text, legend4_rect)
    
    fps_text = font.render(fps)
    fps_rect = fps_text.get_rect()
    fps_rect.topright = (window.get_width() - 10, 20)
    window.blit(fps_text, fps_rect)
    
    pygame.display.update()
    dt += clock.tick()
    frames += 1
    if dt >= 500:
      fps = str(frames)
      dt -= 1000
      frames = 0
    
  return pygame.quit()

if __name__ == '__main__':
  main()