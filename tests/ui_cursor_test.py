﻿#!/usr/bin/env python
"""
Test program for the ui.cursor module.
"""

from __future__ import division

__author__    = 'Orson Maxwell'
__copyright__ = 'Copyright 2014, Orson Maxwell a.k.a. Tech Heathen'
__credits__   = ['Orson Maxwell']
__license__   = 'CC0'
__version__   = '0.0.1'
__date__      = '24.09.2014'
__maintainer__= 'Orson Maxwell'
__email__     = 'orson@techheathen.com'
__status__    = 'Performance test'

from sys import path
from os.path import abspath
from os.path import isfile
from os.path import join
from os import listdir
from unicodedata import name as uname
path.append(abspath('..'))

import pygame
from pygame.locals import *

from ui import *

def main():
  pygame.init()
  
  clock = pygame.time.Clock()
  
  window = pygame.display.set_mode((1024, 768))
  
  font_dir = './data/fonts'
  fonts = [ffile for ffile in listdir(font_dir) if isfile(join(font_dir, ffile))]
  
  if fonts != []:
    font = zipfont.ZipFont(join(font_dir, fonts[0]),
                ignore_missing=True)
  else:
    print('No fonts found! This test requires operational ZipFont facility!')
    return
  
  available_glyphs = sorted(font.glyphs.keys())
    
  fps = '0'
  frames = 0
  dt = 0
  
  pygame.key.set_repeat(300, 60)
  
  cursor_glyph = 0
  
  c = cursor.Cursor((8, 16), font.glyphs[available_glyphs[cursor_glyph]].raw_string)
  
  visible = True
  
  while True:
    window.fill((0, 0, 0, 255))
   
    event = pygame.event.poll()
    
    if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
      break
    elif event.type == MOUSEBUTTONUP:
      if event.button == 3:
        c.toggle()
      elif event.dict['button'] == 5: # Mouse wheel down
        if cursor_glyph > 0:
          cursor_glyph -= 1
        c.set(font.glyphs[available_glyphs[cursor_glyph]].raw_string)
      elif event.dict['button'] == 4: # Mouse wheel up
        if cursor_glyph < len(available_glyphs) - 1:
          cursor_glyph += 1
        c.set(font.glyphs[available_glyphs[cursor_glyph]].raw_string)

    legend1_text = font.render(u'Press ESC to exit')
    legend1_rect = legend1_text.get_rect()
    legend1_rect.topleft = (10, 20)
    window.blit(legend1_text, legend1_rect)
    
    try: # Ignore unnamed char errors
      char_name = u' - ' + uname(unichr(available_glyphs[cursor_glyph]))
    except:
      char_name = u''
    
    legend2_text = font.render(u'Scroll mouse wheel UP and DOWN to change the glyph: ' +
                    u'{} ({}){}'.format(unichr(available_glyphs[cursor_glyph]),
                    format(available_glyphs[cursor_glyph], '04x').upper(),
                    char_name))
    legend2_rect = legend2_text.get_rect()
    legend2_rect. topleft = (10, 36)
    window.blit(legend2_text, legend2_rect)
        
    fps_text = font.render(fps)
    fps_rect = fps_text.get_rect()
    fps_rect.topright = (window.get_width() - 10, 20)
    window.blit(fps_text, fps_rect)
    
    pygame.display.update()
    dt += clock.tick()
    frames += 1
    if dt >= 500:
      fps = str(frames)
      dt -= 1000
      frames = 0
    
  return pygame.quit()

if __name__ == '__main__':
  main()