﻿#!/usr/bin/env python
"""
Daemoncore package test program.
"""

__author__    = 'Orson Maxwell'
__copyright__ = 'Copyright 2013, Orson Maxwell a.k.a. Tech Heathen'
__credits__   = ['Orson Maxwell']
__license__   = 'CC0'
__version__   = '0.0.1'
__date__      = '25.10.2013'
__maintainer__= 'Orson Maxwell'
__email__     = 'orson@techheathen.com'
__status__    = 'Performance test'

from sys import path
from os.path import abspath
path.append(abspath('../..'))

import daemoncore

def main():
  engine = daemoncore.Daemoncore(log_level = 'debug')
  return 0

if __name__ == '__main__':
  main()