Daemoncore
=========

Daemoncore is a Roguelike-like RPG game engine with a twist.
Please visit our [wiki](https://bitbucket.org/orson_maxwell/daemoncore/wiki/Home) for more information.

Prerequisites:
------------

This program requires the following software to run

 - Python 2.7.5
 - PyGame 1.9.2

License:
-------

Daemoncore is licensed under the terms of CC0 1.0 Universal license.
Please see LICENSE.TXT for details.