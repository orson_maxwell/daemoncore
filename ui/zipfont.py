﻿#!/usr/bin/env python
"""
ZipFont module.
"""
from os import path
from zipfile import ZipFile
from pygame import Surface
from pygame.transform import smoothscale
from pygame.transform import scale as roughscale
from pygame import Rect
from pygame import Color
from pygame.image import fromstring
from pygame.locals import *

__all__ = [
  'ZipFontGlyph',
  'ZipFont'
]

class ZipFontGlyph(object):
  """Bitmap ZipFontGlyph."""
  
  def __init__(self, char, surface, raw_string):
    self.data = surface
    self.size = self.data.get_size()
    
    if isinstance(char, int):
      self.char = char
    else:
      raise ValueError('Glyph mapping must be an integer value!')
    
    self.crop = self.data.get_bounding_rect()
    
    if isinstance(raw_string, str):
      self.raw_string = raw_string
    else:
      raise ValueError('Raw string must be a string type value!')
  
class ZipFont(object):
  """Bitmap monospaced font."""
  
  def __init__(self, filename, ignore_missing = False):
    """Load zipfont from filename."""
    
    dir = path.dirname(path.abspath(filename))
    file = path.basename(filename)
    
    self.ignore_missing = ignore_missing
    
    self.zip = ZipFile(path.abspath(filename), 'r')
    
    self.typeface = self.zip.read('typeface.txt')
    
    if 'bold' in self.zip.read('fx.txt'):
      self.bold = True
    else:
      self.bold = False

    if 'italic' in self.zip.read('fx.txt'):
      self.italic = True
    else:
      self.italic = False

    self.glyphs = {}
    
    if not 'size.txt' in self.zip.namelist():
      raise ValueError('No dimension file found in the archive!')
    else:
      size_txt = self.zip.read('size.txt')
    self.size = (int(size_txt.partition(',')[0]),
                 int(size_txt.partition(',')[2]))
    
    for name in self.zip.namelist():
      if name != 'size.txt' and name != 'typeface.txt' and name != 'fx.txt':
        char = int(name.partition('.')[0], base = 16)
        raw_string = self.zip.read(name)
        surface = fromstring(raw_string, self.size, 'RGBA')
        self.glyphs[char] = ZipFontGlyph(char, surface, raw_string)
    self.zip.close()
  
  def render(self, text, colour = None, scale = None,
            back_fill = None, back_tile = None, back_tile_colour = None):
    """Returns a pygame surface with text rendered onto it."""

    text = text.partition('\n')[0] # Don't care about multiline text
    plate = Surface((self.size[0] * len(text), self.size[1]), 
            flags=SRCALPHA, depth = 32)
    
    # Painting the background
    if back_fill:
      plate.fill(back_fill)
    
    char_rect = Rect((0, 0), self.size)
    
    back_tile_surf = Surface(self.size, flags=SRCALPHA, depth=32)
    
    # Tiling the background
    if back_tile and back_tile_colour:
      if ord(back_tile) in self.glyphs.keys():
        back_tile_surf = self.glyphs[ord(back_tile)].data.copy()
        back_tile_surf.fill(back_tile_colour, None, BLEND_RGBA_MULT)

    if not colour:
      colour = (255, 255, 255, 255)

    char_surf = Surface(self.size, flags=SRCALPHA, depth=32)
    
    for char in text:
      # Unsupported char returns empty surface if ignore_missing is on
      if not ord(char) in self.glyphs.keys():
        if not self.ignore_missing:
          raise AttributeError('Character {} missing from font {}!'.format(
                format(ord(char), '04x'), self.typeface))
          char_surf.fill((0, 0, 0, 0))
      else:
        char_surf = self.glyphs[ord(char)].data.copy()
      char_surf.fill(colour, None, BLEND_RGBA_MULT)
      if back_tile and back_tile_colour:
        plate.blit(back_tile_surf, char_rect)
      plate.blit(char_surf, char_rect)
      char_rect = char_rect.move(self.size[0], 0)
    
    # Applying two stage scale for better looks and effeciency
    if scale:
      rough_ratio = int(scale // 1)
      smooth_ratio = scale % 1
      
      if rough_ratio > 1:
        rough_width = plate.get_size()[0] * rough_ratio
        rough_height = plate.get_size()[1] * rough_ratio
        scaled_plate = roughscale(plate, (rough_width, rough_height))
      else:
        scaled_plate = plate
      
      if smooth_ratio > 0.01: # Neglecting floating point division error
        smooth_width = int(plate.get_size()[0] * scale)
        smooth_height = int(plate.get_size()[1] * scale)
        scaled_plate = smoothscale(scaled_plate, (smooth_width, smooth_height))
      return scaled_plate

    return plate
