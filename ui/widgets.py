#!/usr/bin/env python
"""
Widgets module.
"""

from pygame.color import Color

class Widget(object):
  def __init__(self):
    self.width = 0
    self.height = 0
    self.order = 0
    self.border = ''
    self.border_color = Color()
    self.caption = ''
