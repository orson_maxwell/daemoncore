﻿#!/usr/bin/env python
"""
Cursor module.
"""
from pygame import mouse
from pygame import cursors
from pygame.locals import *

__all__ = [
  'Cursor',
]

class Cursor(object):
  """Bitmap mouse pointer."""
  
  def __init__(self, size, raw_string, visible=True):
    """Initialize the cursor."""
    
    if (size[0] % 8 != 0) or (size[1] % 8 != 0):
      raise ValueError('Cursor size must be a multiple of 8 in each dimension!')
    else:
      self.size = size

    self.visible = visible
    self.set(raw_string)
  
  def set(self, raw_string):
    """Split the raw RGBA string into unicode lines of given size and set the
    cursor shape"""
    strings = []
    for y in range(0, self.size[1]):
      strings.append(u'')
      for x in range(0, self.size[0] * 4, 4):
        strings[y] += unichr(ord(raw_string[y * self.size[0] * 4 + x]))
    
    cursor, mask = cursors.compile(strings, white = None, black = None, xor = u'\u00ff')
    mouse.set_cursor((self.size[0], self.size[1]), (0,0), cursor, mask)
  
  def toggle(self):
    self.visible = not self.visible
    mouse.set_visible(self.visible)
  
  def show(self):
    self.visible = True
    mouse.set_visible(self.visible)
    
  def hide(self):
    self.visible = False
    mouse.set_visible(self.visible)
