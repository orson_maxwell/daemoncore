Roguelike TTF font version 1.30
Author: Orson Maxwell
License: Creative Commons 0 (public domain)

Greetings!
This font is a compilation of a few of my most favourite bitmap fonts that are
used for console teminals and VGA displays, but in TTF format.
It is primarily based on the IBM VGA terminal bitmap font, but includes some
notable additions and differences I made to my taste. Here they are:

Changed glyphs to improve looks and readability: 3, 8, $, cent, double
  exclamation, and shaded block symbols

I also added stylized glyphs for some of the more common unicode characters -
see below.

Fully supported unicode blocks:
  - Basic Latin
  - Latin-1 Supplement
  - Latin Extended-A
  - Box Drawing
  - Block Elements
  - Subscripts and superscripts
  - Number Forms
  - Hiragana

Partially supported unicode blocks:
  - Latin Extended-B (11/208)
  - Spacing Modifier Letters (9/80)
  - Combining Diacritical Marks (4/112)
  - Greek and Coptic (75/134)
  - Cyrillic (104/256 - mostly Russian)
  - Latin Extended Additional (8/256)
  - General Punctuation (20/111)
  - Currency Symbols (5/27)
  - Letterlike Symbols (5/80)
  - Number Forms (6/58)
  - Arrows (7/112)
  - Mathematical operators (9/256)
  - Miscellaneous Technical (4/244)
  - Geometric shapes (12/96)
  - Miscellaneous symbols (11/256)
  - Alphabetic Presentation Forms (2/58 - fi and fl ligatures)

Unicode blocks that are planned to be supported (fully or partially):
  - Greek and Coptic (full Greek support)
  - Currency Symbols (full support)
  - Runic (full support)
  - Mathematical operators (fuller support)
  - Geometric shapes (fuller support)

This font should be rendered at typographic sizes which a multiples of 12pt.
At 12pt this font occupies a 16x8 monospace.

For any questions or symbol support requests feel free to contact me via email:
  orson.jules.maxwell@gmail.com

VERSION HISTORY:
  - 1.20 - First release
  - 1.30 - Added full support for:
           - Subscripts and Superscripts
           - Number Forms
           - Hiragana (full support)
         - Some minor tweaks