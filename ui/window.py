#!/usr/bin/env python
"""
Window module.
"""

import pygame
from pygame.display import set_mode
from pygame.display import set_caption
from pygame.display import set_icon
from pygame.image import load
from pygame.locals import *

__all__ = [
  'Window'
]

class Window(object):
  """Main game window class."""
  
  def __init__(self, size=(1024, 768), fullscreen=False, caption='Daemoncore',
              hardware=False, icon=None):

    self.width = size[0]
    self.height = size[1]
    self.caption = caption
    self.fullscreen = fullscreen
    
    pygame.init()
    
    flags = 0
    
    if icon:
      try:
        icon_surf = load(icon)
      except:
        pass
      else:
        set_icon(icon_surf)
    
    if fullscreen:
      flags = flags | FULLSCREEN
    
    self.surface = set_mode((self.width, self.height), flags)
    set_caption(caption)
