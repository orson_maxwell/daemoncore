#!/usr/bin/env python
"""
Render a TTF font to a bitmap zipfont usable by Daemoncore.
"""

from __future__ import division

__author__    = 'Orson Maxwell'
__copyright__ = 'Copyright 2013, Orson Maxwell a.k.a. Tech Heathen'
__credits__   = ['Orson Maxwell']
__license__   = 'All rigths reserved'
__version__   = '1.0.0'
__date__      = '21.10.2013'
__maintainer__= 'Orson Maxwell'
__email__     = 'orson@techheathen.com'
__status__    = 'Functional'

import argparse
from os import remove
from os import path
from sys import stdout
from zipfile import ZipFile
from zipfile import ZIP_DEFLATED

try:
  from pygame import init
  from pygame import quit
  from pygame.ftfont import Font
  from pygame.image import tostring
  from pygame.image import save
  from pygame import Surface
  from pygame import Rect
  from pygame.locals import SRCALPHA
except ImportError:
  print('This program requires PyGame to run!')
  raise

# Let us define the unicode blocks and glyphs they contain:
unicode_blocks = {
  'latin': range(0x0020, 0x007E) +\
          range(0x00A1, 0x00AD) +\
          range(0x00AE, 0x0180),
  'ipa': range(0x0250, 0x02B0),
  'comb_dia': range(0x0300, 0x033F) +\
          range(0x0035F, 0x00370),
  'greek': range(0x0370, 0x0378) +\
          range(0x037B, 0x037E) +\
          range(0x0391, 0x03A2) +\
          range(0x03A3, 0x03E2),
  'cyrillic': range(0x0400, 0x0528),
  'runic': range(0x16A0, 0x16F1),
  'punct': [0x2010] +\
          range(0x2012, 0x2028) +\
          range(0x2030, 0x205F),
  'sub_super': range(0x2070, 0x2072) +\
          range(0x2074, 0x208F) +\
          range(0x2090, 0x209D),
  'currency': range(0x20A0, 0x20BB),
  'math': range(0x2200, 0x2300),
  'misc_tech': range(0x2300, 0x23F4),
  'boxes': range(0x2500, 0x2580),
  'blocks': range(0x2580, 0x25A0),
  'hiragana': range(0x3041, 0x3097) +\
          range(0x3099, 0x30A0),
  'katakana': range(0x30A0, 0x3100)
}
        
def main():
  parser = argparse.ArgumentParser(description='Create a zipped bitmap font.')
  parser.add_argument('-f', '--font', required=True,
                    help='a TTF font name to use')
  parser.add_argument('-s', '--size', type=int, required = True,
                    help='at what size to render the font')
  parser.add_argument('-u', '--unicode-blocks', action='append', default=[],
                    help='what unicode blocks to render',
                    choices = ['latin',
                              'ipa',
                              'comb_dia',
                              'greek',
                              'cyrillic',
                              'runic',
                              'punct',
                              'sub_super',
                              'currency',
                              'math',
                              'misc_tech',
                              'boxes',
                              'blocks',
                              'hiragana',
                              'katakana',
                              'all'])
  parser.add_argument('-o', '--output', default = '',
                    help='use antialiasing when rendering')
  parser.add_argument('-b', '--bold', action='store_true', default=False,
                    help='apply bold font type')
  parser.add_argument('-i', '--italic', action='store_true', default=False,
                    help='apply italic font type')
  parser.add_argument('-v', '--verbose', action='store_true', default=False,
                    help='increase output verbosity')
  parser.add_argument('-q', '--quiet', action='store_true', default=False,
                    help='suppress all output')
  args = parser.parse_args()
  
  init()
  try:
    font = Font(args.font, args.size)
  except:
    print('Failed to load font: {}'.format(args.font))
    quit()
    return 0

  dir = path.dirname(path.abspath(args.font))
  file = path.basename(args.font)
    
  font.strong = args.bold
  font.oblique = args.italic
  
  if args.unicode_blocks == []:
    args.unicode_blocks += ['all']

  glyphs_to_render = []
  if 'all' in args.unicode_blocks:
    for block in unicode_blocks.keys():
      glyphs_to_render += unicode_blocks[block]
  else:
    for block in args.unicode_blocks:
      glyphs_to_render += unicode_blocks[block]

  # Get the actual size
  # PyGame adds an empty line of pixels at the bottom of the text surface
  max_width = 0
  max_height = 0
  for glyph in glyphs_to_render:
    max_width = max(max_width, font.size(unichr(glyph))[0])
    max_height = max(max_width, font.size(unichr(glyph))[1] - 1)
  size = (max_width, max_height)
  crop = Rect((0, 0), size)
  
  if args.verbose:
    print('Rendering {} at {}x{} ({}pt)...'.format(args.font, size[0], size[1],
                                                  args.size))
    if font.fixed_width:
      print('This font is monospaced.')
    if font.strong:
      print('Using bold type.')
    if font.oblique:
      print('Using italic type.')

  if args.output == '':
    args.output = path.join(dir, font.name) + '_' +\
                  str(size[0]) + 'x' + str(size[1])
    if args.bold:
      args.output += '_b'
    if args.italic:
      args.output += '_i'
    args.output += '.zip'
  
  if args.output[-4:] == '.zip':
    zip_name = args.output
  else:
    zip_name = args.output + '.zip'
  try:
    zip = ZipFile(zip_name, 'w', ZIP_DEFLATED)
  except:
    print('Cannot write to path: {}'.format(zip_name))
    quit()
    return(0)
    
  progress = 1
  glyphs_corrected = 0
  
  zip.writestr('size.txt', '{},{}'.format(size[0], size[1]))
  zip.writestr('typeface.txt', font.name)
  fx_string = ''
  if args.bold:
    fx_string += 'bold'
    if args.italic:
      fx_string += ',italic'
  elif args.italic:
    fx_string += 'italic'
  zip.writestr('fx.txt', fx_string)
  
  # Render and zip 'em!
  for glyph in glyphs_to_render:
    if not args.quiet:
      stdout.write('\r{:.2f}%'.format(progress / len(glyphs_to_render) * 100))
    
    surface = font.render(unichr(glyph), True, (255, 255, 255, 255))
    
    # Some character vary in width even in some monospaced fonts...
    if surface.get_rect().w != crop.w:
      glyphs_corrected += 1
      new_surface = Surface(crop.size, depth=32, flags=SRCALPHA)
      new_surface.blit(surface, crop)
      surface = new_surface
    
    if surface.get_bounding_rect().w > 0 or glyph == 32:
      image_name = format(glyph, '04x').upper() + '.raw'
      # I have no idea why certain chars in certain fonts render without
      # per pixel alpha...
      try:
        zip.writestr(image_name, tostring(surface.subsurface(crop), 'RGBA_PREMULT'))
      except ValueError:
        surface.set_alpha(None)
        # Oh yeah I love this
        for x in range(size[0]):
          for y in range(size[1]):
            colour = surface.get_at((x, y))
            colour.a = colour.r
            print colour
            surface.set_at((x, y), colour)
        if glyph == 0x006D:
          print surface.get_at((0, 0))
        zip.writestr(image_name, tostring(surface.subsurface(crop), 'RGBA'))
    progress += 1

  if args.verbose:
    print('\nRendered {} glyphs total, corrected width for {}.'.format(
          len(glyphs_to_render), glyphs_corrected))
    print('Output to {}'.format(zip_name))
    
  zip.close()

  quit()
  return 0
  
if __name__ == '__main__':
  main()