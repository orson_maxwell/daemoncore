#!/usr/bin/env python
"""Platform dependant routines go here."""

__all__ = [
  'get_screen_size',
  'os_id',
  'os_name',
  'py_release',
  'py_version',
]

import os
import sys
import platform
import ctypes

# What are we running on?
os_id = platform.platform()
os_name = sys.platform
py_release = platform.python_implementation()
py_version = platform.python_version()
os_name = sys.platform

# Wrap useful libs
if 'win32' in os_name:
  user32 = ctypes.windll.user32

def get_screen_size(failsafe = (800, 600)):
  """Get the desktop resolution."""
  
  try:
    return user32.GetSystemMetrics(0), user32.GetSystemMetrics(1)
  except:
    return failsafe
  # Add portability here please!
